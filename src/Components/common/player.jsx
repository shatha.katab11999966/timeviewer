import React, {Component} from 'react';


export default class Player extends Component {

    constructor(props) {
        super(props);
        document.title = "Time Viewer";
    }


    render() {
        return <div>
            <video src={'https://s3.amazonaws.com/codecademy-content/courses/React/react_video-fast.mp4'}  controls autoPlay/>
        </div>
    }

}