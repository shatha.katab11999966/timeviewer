import React, {Component} from 'react';
import {Image, Grid , Rating , Tab} from 'semantic-ui-react';
import '../style.css';
import Intro from "./intro";


export default class MainLayout extends Component {

    constructor(props) {
        super(props);
        document.title = "Time Viewer";
    }




    render() {

        const panes = [
            {
                menuItem: 'نبذة عن هند', render: () => <Tab.Pane className={'tabs-content'} attached={false}>
                    <Intro />
                </Tab.Pane>

            },
            {
                menuItem: 'متجر',

            },
            {
                menuItem: 'برودكاست',

            },
            {
                menuItem: 'كورسات',

            },
            {
                menuItem: 'حجز عيادة (20 دينار كويتي)',

            },
        ]
        return <Grid className={"main-page"}>
            <Grid.Row className={"header-menu"}>
                <Image className={'header-icon'} src={`img/notification-icon.png`}/>
                <div className={'login-icon'}><Image className={'user-icon'} src={`img/user-icon.png`}/></div>
                <div className={'welcome-text'}>
                    <span className={'dark-blue-text-bold'}> مرحبا بك</span>
                     <div className={'dark-blue-text'}>مالك محمد  </div>
                </div>
            </Grid.Row>
            <Grid.Row className={'secondary-menu'}>
                <Image className={'logo'} src={`img/logo.png`} />
                <Image className={'home-icon'} src={`img/home-icon.png`} />
                <div className={'dr-info'}>
                    <Image className={'profile-pic'} src={`img/profile.png`} />
                    <div className={'personal-info'}>
                        <div className={'bold-white-text'}>العيادة الرقمية</div>
                        <div className={'d-flex status-container'}>
                            <div className={'cloudy-blue-text '}>ل د. هند الناهض</div>
                            <div className={'status'}>مشغول باستشارة</div>
                        </div>
                        <div className={'d-flex'}>
                            <Rating  className={'stars'}  icon='star' defaultRating={3} maxRating={4} />
                            <div className={'small-text'}>تقييم</div>
                        </div>
                    </div>

                </div>

            </Grid.Row>
            <Grid.Row className={'tabs-row'}>
                <div className={'tabs-section'}>
                    <Tab className={'tabs'} menu={{ secondary: true , attached: false} } panes={panes} />
                </div>

            </Grid.Row>

        </Grid>
    }
}