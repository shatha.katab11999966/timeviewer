import React, {Component} from 'react';
import ReactPlayer from 'react-player'
import '../style.css';
import {Grid, Image, Rating, Tab} from "semantic-ui-react";
import Player from "./common/player";


export default class Intro extends Component {

    constructor(props) {
        super(props);
        document.title = "Time Viewer";
    }


    render() {
        return <Grid>
            <Grid.Row>
            <Grid.Column mobile={16} tablet={7} computer={7}>
                <div className={'intro-text'}>
                    هند الناهض مستشارة إعلام رقمي, مستشارة الإعلام الإلكتروني لمكتب وكيل وزارة الإعلام الكويتية وهي
                    المؤسسة لشركة “سوشالوبي” للخدمات المختصة في مجال الإعلام الإجتماعي , بالإضافة الى إنها مستشارة في
                    مجال التواصل الاجتماعي.هند الناهض مستشارة إعلام رقمي, مستشارة الإعلام الإلكتروني لمكتب وكيل وزارة
                    الإعلام الكويتية وهي المؤسسة لشركة “سوشالوبي” للخدمات المختصة في مجال الإعلام الإجتماعي , بالإضافة
                    الى إنها مستشارة في مجال التواصل الاجتماعي.
                </div>
                <div className={'subtitle'}>
                    الخبرات
                </div>
                <div className={'tags-section d-flex'}>
                    <div className={'tag'}>تطوير الأعمال</div>
                    <div className={'tag'}>المشاريع الصغيرة</div>
                    <div className={'tag'}>مشاركة العملاء</div>
                    <div className={'tag'}>الإعلام</div>
                    <div className={'tag'}>التحدث أمام الجمهور</div>
                    <div className={'tag'}>الدعاية والإعلان</div>
                    <div className={'tag'}>ريادة الأعمال</div>
                    <div className={'tag'}>القيادة</div>
                    <div className={'tag'}>التسويق الرقمي</div>
                </div>
            </Grid.Column>
            <Grid.Column mobile={16} tablet={9} computer={9}>
                <Player   />

            </Grid.Column>
            </Grid.Row>
        </Grid>
    }

}