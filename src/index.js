import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { Route, Router, Switch , browserHistory} from 'react-router';
import 'semantic-ui-css/semantic.min.css';
import MainLayout from "./Components/mainlayout";
import { createBrowserHistory } from "history";
const history = createBrowserHistory();
ReactDOM.render(
  <React.StrictMode>
      <Router history={history} >
          <Switch>
              <Route path="/">
                  <MainLayout />
              </Route>
          </Switch>
      </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
